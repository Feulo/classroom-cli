# Classroom-CLI

A CLI for Google Classroom management

## Install

To use Classroom-CLI you'll need to have python3.6+ and install the following python modules:

`pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib`

## Usage

To use the Classroom-CLI just add the path to the .py file in your system path or create a alias.

[here](https://accounts.google.com/ServiceLogin/signinchooser?passive=true&service=ahsid&continue=https%3A%2F%2Fdevelopers.google.com%2Fclassroom%2Fquickstart%2Fpython%3Frefresh%3D1&flowName=GlifWebSignIn&flowEntry=ServiceLogin)
