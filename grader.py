from flake8.api import legacy as flk8
from contextlib import redirect_stdout
import traceback
import io
from timeout_decorator import timeout
import pickle
import importlib.util as imp
import shutil
from itertools import zip_longest
import os
import sys


class PenaltieError(Exception):
    pass


def check(test_name, test_points):
    @timeout(10, use_signals=False)
    def timeout_execution(self, func):
        func(self)

    def decorator(test):
        def wrapper(self):
            p = "* Usou 'input' ou 'print' sem que fosse pedido no enunciado:"
            with io.StringIO() as out, redirect_stdout(out):
                try:
                    timeout_execution(self, test)
                except AssertionError:
                    self.summary[f'* {test_name}:'] = (0, test_points)
                    self.trace += [traceback.format_exc()]
                except Exception:
                    self.summary[f'* {test_name}:'] = (0, test_points)
                    self.trace += [traceback.format_exc()]
                else:
                    self.summary[f'* {test_name}:'] = (test_points,
                                                       test_points)
                if out.getvalue():
                    self.penalties[p] = -1
        return wrapper
    return decorator


class BaseGrader:

    def __init__(self, path):
        # alterar para colocar path em info
        self.path = path
        self.grade = 0
        self.trace = []
        self.summary = {}
        self.penalties = {}
        self.file = os.path.join(self.path, self.filename)
        with open(os.path.join(self.path, '.info'), 'rb') as i:
            self.info = pickle.load(i)
        print(' e '.join(self.info.nomes))

    def import_ac(self):
        with io.StringIO() as buf, redirect_stdout(buf):
            try:
                spec = imp.spec_from_file_location('ac',
                                                   self.file)
                self.ac = imp.module_from_spec(spec)
                spec.loader.exec_module(self.ac)
            except Exception as err:
                t = '* Erros de sintaxe (não será corrigido):'
                self.penalties[t] = -10
                print(err, file=sys.stderr)
                raise(PenaltieError)
            else:
                pass
            self.user_output = buf.getvalue()
            if self.user_output:
                p = "* Código executado na importação do módulo:"
                self.penalties[p] = -1

    def authors_check(self):
        pass
        #if len(self.info.emails) < 2:
        #    self.penalties['* Entregou Individual (Não será corrigido):'] = -10
        #    raise PenaltieError
        #if len(self.info.sub_ids) < 2:
        #    self.penalties['* Somente 1 da dupla entregou a AC:'] = -3

    def files_check(self):
        if self.filename not in self.info.files:
            self.penalties['* Renomeou os arquivos:'] = -3
            if len(self.info.files) == 1:
                f = list(self.info.files)[0]
                shutil.copy(os.path.join(self.path, f), self.file)

        if len(self.info.files) > 1:
            p = ('* Entregar arquivos diferentes do '
                 'específicado (-0.5 por arquivo):')
            self.penalties[p] = (len(self.info.files) - 1) * -0.5

    def style_check(self):
        self.summary['* Padrão de código:'] = (0, 1.5)
        with io.StringIO() as f, redirect_stdout(f):
            style_guide = flk8.get_style_guide(help=True)
            report = style_guide.check_files([self.file])
            if report.total_errors == 0:
                self.summary['* Padrão de código:'] = (1.5, 1.5)
            self.style_errors = f.getvalue()

    def create_log(self, filename="README.md"):
        filename = os.path.join(self.path, filename)
        with open(filename, 'w') as log:
            log.write(f"# {self.ac_name}\n\n")
            log.write("## Alunos:\n\n")
            lista = zip_longest(sorted(self.info.nomes),
                                sorted(self.info.emails),
                                fillvalue='Aluno sem entrega')
            for aluno, email in lista:
                log.write(f"{aluno} ({email})\n")
            log.write("\n## Resumo:\n\n")
            for item, value in self.summary.items():
                log.write(f"{item:66}{value[0]}/{value[1]}\n")
            for item, value in self.penalties.items():
                log.write(f"{item:66}{value}\n")
            log.write(f"\n{'TOTAL':66}{self.grade}\n\n")
            log.write("\n## Arquivos enviados\n\n")
            for f in self.info.files:
                log.write(f"\t- `{f}`\n")
            log.write("\n## Erros de Padrão de Código:\n\n```\n")
            log.write(getattr(self, 'style_errors', ''))
            log.write("```\n\n## Saidas do módulo do aluno:\n\n```\n")
            log.write(getattr(self, 'user_output', ''))
            for tr in self.trace:
                log.write(f"{tr}\n")
            log.write("```")

    def write_csv(filename):
       pass

    def sum_grade(self):
        for nota in self.summary.values():
            self.grade += nota[0]
        for penaltie in self.penalties.values():
            self.grade += penaltie
        self.grade = max(self.grade, 0)

    def grade_sub(self):
        # Em Caso de Erro que impede  a correção fazer o break
        try:
            self.files_check()
            self.authors_check()
            self.style_check()
            self.late_check()
            self.import_ac()
        except PenaltieError:
            pass
        except Exception as err:
            print(err)
        else:
            for func in dir(self):
                if func.startswith('check_'):
                    meth = getattr(self, func)
                    meth()
        finally:
            self.sum_grade()
            self.create_log()
            return self.grade

    def late_check(self):
        if self.info.late:
            self.penalties['* Entregou atrasado (até 20 min):'] = -2
            # raise PenaltieError
