#!/usr/bin/env python3
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import sys
import argparse
# import argcomplete
import importlib.util as imp
import os
import re
import json
import shutil
from zipfile import ZipFile, is_zipfile
# from contextlib import redirect_stdout

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/classroom.coursework.students',
          'https://www.googleapis.com/auth/classroom.coursework.me',
          'https://www.googleapis.com/auth/classroom.courses',
          'https://www.googleapis.com/auth/classroom.rosters',
          'https://www.googleapis.com/auth/classroom.announcements',
          'https://www.googleapis.com/auth/classroom.courses',
          'https://www.googleapis.com/auth/classroom.rosters',
          #  'https://www.googleapis.com/auth/classroom.rosters.readonly',
          'https://www.googleapis.com/auth/classroom.profile.emails',
          #  'https://www.googleapis.com/auth/classroom.profile.photos',
          'https://www.googleapis.com/auth/drive']

CRED_PATH = os.path.expandvars('$HOME/.classroom/')
TOKEN_FILE = os.path.expandvars('$HOME/.classroom/token.pickle')


class ClassroomCli():

    @staticmethod
    def create_folder():
        if not os.path.exists(CRED_PATH):
            os.makedirs(CRED_PATH)

    @staticmethod
    def _get_cred_id(credential):
        if not os.path.exists(CRED_PATH + credential):
            raise Exception('credential not found')
        with open(CRED_PATH + credential) as cred:
            creds = json.load(cred)
        return creds['installed']['client_id']

    @staticmethod
    def _get_token_id():
        if not os.path.exists(TOKEN_FILE):
            raise Exception('No token')
        with open(TOKEN_FILE, 'rb') as token:
            creds = pickle.load(token)
        return creds.client_id

    @staticmethod
    def _log(acc_name):
        flow = InstalledAppFlow.from_client_secrets_file(CRED_PATH + acc_name,
                                                         SCOPES)
        creds = flow.run_local_server(port=8888)
        # Save the credentials for the next run
        with open(TOKEN_FILE, 'wb') as token:
            pickle.dump(creds, token)

    def account(self, args):
        if args.switch_to:
            self._log(args.switch_to)
        elif args.add:
            shutil.copy(args.add[1], CRED_PATH + args.add[0])
            self._log(args.add[0])
        elif args.remove:
            if self._get_cred_id(args.remove) == self._get_token_id:
                os.remove(TOKEN_FILE)
            os.remove(CRED_PATH + args.remove)
            print(f"account {args.remove} removed.")
        else:
            for _, _, files in os.walk(CRED_PATH):
                if not files:
                    print("You have no accounts stored.")
                else:
                    print(f'{"ACCOUNT":15}ACTIVE')
                    tk = self._get_token_id()
                    for f in files:
                        if not f == 'token.pickle':
                            c_id = self._get_cred_id(f)
                            print(f'{f:15}{"*" if tk == c_id else ""}')

    def _get_creds(self):
        """
        Get credencial and updates token.picke for user OAuth2
        """
        if not os.path.exists(TOKEN_FILE):
            raise Exception('No active account, please choose/add an account')
        with open(TOKEN_FILE, 'rb') as token:
            creds = pickle.load(token)
        if not creds.valid:
            if creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                raise Exception('Deu Ruim')
        return creds

    class _Curso:
        def __init__(self, obj):
            sec = ['']*10
            for i, w in enumerate(obj.get('section', 'a b c d e').split()):
                sec[i] = w
            self.semestre = sec[0]
            self.turma = sec[2] + sec[3]
            self.periodo = sec[4]
            self.nome = obj['name']
            self.id = obj['id']
            self.alias = ""

        def add_alias(self, req_id, response, exception):
            als = response.get('aliases', {})
            for al in als:
                if al['alias'][0] == 'p':
                    self.alias = al['alias']

        def __str__(self):
            return f"{self.id:13} {self.nome:30} {self.turma:10} {self.alias}"

    def courses(self, args):
        cursos = {}
        results = self.service.courses()
        fds = 'courses(id,name,section)'
        if args.all:
            results = results.list(fields=fds)
        else:
            results = results.list(courseStates='ACTIVE', fields=fds)
        courses = results.execute().get('courses', [])
        batch = self.service.new_batch_http_request()
        for course in courses:
            cursos[course['id']] = self._Curso(course)
            response = (self.service.courses().aliases()
                        .list(courseId=course['id']))
            batch.add(response, callback=cursos[course['id']].add_alias)
        batch.execute()
        print(f"{'ID':13} {'NOME':30} {'TURMA':10} ALIASES")
        for curso in cursos.values():
            print(curso)

    def works(self, args):
        results = self.service.courses().courseWork()
        results = results.list(courseId=args.course,
                               courseWorkStates=['PUBLISHED', 'DRAFT'],
                               fields='courseWork(id,title,state)')
        print(f"{'ID':15} {'STATE':11} TITLE")
        for work in results.execute().get('courseWork', []):
            print(f"{work['id']:15} {work['state']:11} {work['title']}")

    def email(self, args):
        results = self.service.userProfiles().get(userId=args.id_aluno)
        results = results.execute()
        print(results)

    def alias(self, args):
        results = self.service.courses().aliases()
        if args.add:
            al = {'alias': args.add}
            results.create(courseId=args.course, body=al).execute()
        if args.remove:
            results.delete(courseId=args.course, alias=args.remove).execute()
        results = results.list(courseId=args.course).execute()
        aliases = [alias['alias'] for alias in results.get('aliases', [])]
        print(f"Aliases: {', '.join(aliases)}")

    def _create_student_table(self, course):
        stu = {}
        tk = None
        while True:
            students = self.service.courses().students().list(courseId=course,
                                                              pageToken=tk)
            students = students.execute()
            tk = students.get('nextPageToken')
            students = students.get('students', {})
            for student in students:
                stu[student['userId']] = student['profile']['name']['fullName']
            if tk is None:
                break
        return stu

    def _get_workid(self, course):
        results = self.service.courses().courseWork()
        results = results.list(courseId=course,
                               courseWorkStates=['PUBLISHED', 'DRAFT'],
                               fields='courseWork(id,title,state)')
        results = sorted(results.execute().get('courseWork', []),
                         key=lambda x: x.get('title'))
        print('{3:2} {0:15} {1:11} {2}'.format('ID', 'STATE', 'TITLE', ''))
        for i, work in enumerate(results):
            print(f"{i:2}) {work['id']:15} {work['state']:11} {work['title']}")
        w = input('\nChoose a work to list submissions: ')
        print()
        return results[int(w)]['id']

    def submissions(self, args):

        stu = self._create_student_table(args.course)
        if not args.workid:
            args.workid = self._get_workid(args.course)
        f = 'studentSubmissions(id,state,late,draftGrade,assignedGrade,userId)'
        results = self.service.courses().courseWork().studentSubmissions()
        lt = None if args.late or args.all else 'NOT_LATE_ONLY'
        if args.all:
            sts = None
        elif args.status:
            sts = args.status
        else:
            sts = 'TURNED_IN'

        results = results.list(courseId=args.course,
                               late=lt,
                               states=sts,
                               courseWorkId=args.workid,
                               fields=f)
        if args.id:
            print(f"{'NOME':30} {'STATE':10}ID ")
            for sub in sorted(results.execute().get('studentSubmissions', []),
                              key=lambda x: stu.get(x.get('userId'))):
                print('{0:30.30s} {1:10}{2}'
                      .format(stu.get(sub.get('userId', ''),
                                      sub.get('userId')),
                              sub.get('state', ''),
                              sub.get('id', '')))
        else:
            print(f"{'NOME':30} {'STATE':10}{'LATE':5}{'DRAFT':6}{'GRADE':6}")

            for sub in sorted(results.execute().get('studentSubmissions', []),
                              key=lambda x: stu.get(x.get('userId'))):
                print('{0:30.30s} {1:10}{2:5}{3:6}{4:6}'
                      .format(stu.get(sub.get('userId', ''),
                                      sub.get('userId')),
                              sub.get('state', ''),
                              'YES' if sub.get('late') else 'NO',
                              sub.get('draftGrade', ''),
                              sub.get('assignedGrade', '')))

    def history(self, args):
        fds = 'studentSubmissions(submissionHistory)'
        results = self.service.courses().courseWork().studentSubmissions()
        results = results.list(courseId=args.course,
                               courseWorkId=args.workid,
                               userId=args.userid,
                               fields=fds)

        for sub in results.execute().get('studentSubmissions', []):
            for action in sub.get('submissionHistory', []):
                print(action)

    @staticmethod
    def _make_fname(emails):
        names = []
        for email in sorted(emails):
            if '@' in email:
                names.append(email.split('@')[0].replace(".", "_"))
            else:
                names.append(email.split('.f')[0].replace(".", "_"))
        return "-".join(names)

    class _SubInfo:
        def __init__(self, nome, emails, course_id, work_id, sub_ids, files,
                     late):
            self.course_id = course_id
            self.work_id = work_id
            self.emails = set(emails) if emails else set()
            self.sub_ids = set(sub_ids)
            self.files = set(files) if files else set()
            self.late = late
            self.nomes = set(nome) if nome else set()

        def __str__(self):
            return f"nomes: {self.nomes}\nemails: {self.emails}\n \
                    workid: {self.work_id}ids: \
                    {self.sub_ids}\nfiles: {self.files}\nlate: {self.late}"

        def __add__(self, other):
            nomes = self.nomes.union(other.nomes)
            emails = self.emails.union(other.emails)
            sub_ids = self.sub_ids.union(other.sub_ids)
            files = self.files.union(other.files)
            late = self.late and other.late
            return ClassroomCli._SubInfo(nomes, emails, self.course_id,
                                         self.work_id, sub_ids, files, late)

    def download(self, args):

        if not args.workid:
            args.workid = self._get_workid(args.course)

        self.students = self._create_student_table(args.course)
        drive = build('drive', 'v3', credentials=self._get_creds())
        # lt = 'LATE_ONLY' if args.late else 'NOT_LATE_ONLY'
        fds = 'studentSubmissions(id,userId,late,assignmentSubmission)'
        sts = args.status if args.status else 'TURNED_IN'
        results = self.service.courses().courseWork().studentSubmissions()
        results = results.list(courseId=args.course,
                               states=sts,
                               courseWorkId=args.workid,
                               fields=fds)
        subs = results.execute().get('studentSubmissions', [])
        for sub in subs:
            print(f"aluno: {self.students.get(sub.get('userId'),'')}")
            if args.zip:
                self._download_zip(sub, drive, args)
            else:
                self._download_files(sub, drive, args)

    def _download_files(self, submission, drive, args):
        al = None
        downloaded = set()
        attachs = [(drfl['driveFile']['id'], drfl['driveFile']['title'])
                   for drfl in submission.get('assignmentSubmission').get('attachments', []) if 'driveFile' in drfl]

        for file_id, filename in attachs:
            try:
                req = drive.files().get_media(fileId=file_id).execute()
                # req = req.execute().decode('utf-8')
            except Exception as err:
                print(filename, err)
            else:
                try:
                    text = req.decode('utf-8')
                except UnicodeDecodeError:
                    text = req.decode('latin1')

                al = al or re.findall(r'\w+\.[\w\d]+@aluno\.[\w\.]+\.br', text)
                with open(filename, 'wb') as arq:
                    arq.write(req)
                downloaded.add(filename)
        print(al)
        print(attachs)
        if al:
            student_set = frozenset(al)
            path = os.getcwd() + f"/{self._make_fname(student_set)}"
        else:
            path = os.getcwd() + f"/{submission['id']}"
        if not os.path.exists(path):
            os.mkdir(path)
        for filename in downloaded:
            shutil.move(filename, f'{path}/{filename}')
        info = self._SubInfo([self.students.get(submission['userId'])], al,
                             args.course, args.workid,
                             [submission['id']], downloaded,
                             submission.get('late', False))
        if not os.path.exists(f"{path}/.info"):
            with open(f"{path}/.info", 'wb') as p:
                pickle.dump(info, p)
        else:
            with open(f"{path}/.info", 'r+b') as p:
                prev_info = pickle.load(p)
                info = prev_info + info
                p.seek(0)
                p.truncate()
                pickle.dump(info, p)

    def _download_zip(self, submission, drive, args):
        # al = None
        downloaded = set()
        attachs = [(drfl['driveFile']['id'], drfl['driveFile']['title'])
                   for drfl in submission.get('assignmentSubmission')
                                         .get('attachments', [])]

        for file_id, filename in attachs:
            try:
                req = drive.files().get_media(fileId=file_id).execute()
                # req = req.execute().decode('utf-8')
            except Exception as err:
                print(filename, err)
            else:
                with open(filename, 'wb') as arq:
                    arq.write(req)
                downloaded.add(filename)
        for filename in downloaded:
            if is_zipfile(filename):
                with ZipFile(filename, 'r') as compacted:
                    try:
                        compacted.extractall('.')
                    except Exception:
                        print("Erro ao extrair arquivo")
        #    shutil.move(filename, f'{path}/{filename}')

    def grade(self, args):
        spec = imp.spec_from_file_location('grader', args.grader)
        mod = imp.module_from_spec(spec)
        spec.loader.exec_module(mod)
        print("AQUI CHEGOU")
        # for subdir, dirs, files in os.walk(os.getcwd()):
        for f in filter(lambda x: os.path.isdir(x) and
                        os.path.isfile(x + '/.info'),
                        os.listdir(os.getcwd())):
            grader = mod.Grader(f)
            grader.grade_sub()

    def pre_grade(self, args):
        for f in filter(lambda x: os.path.isdir(x), os.listdir(os.getcwd())):
            dummy = self._SubInfo("", "", "", "", "", "", False)
            with open(f"{f}/.info", 'wb') as p:
                pickle.dump(dummy, p)

    def test(self, args):
        for f in filter(lambda x: os.path.isdir(x) and not x == '__pycache__',
                        os.listdir(os.getcwd())):
            with open(f"{f}/.info", 'rb') as p:
                info = pickle.load(p)
            results = self.service.courses().courseWork().studentSubmissions()
            results = results.patch(courseId=args.course,
                                    courseWorkId=info.work_id,
                                    id=list(info.sub_ids)[0],
                                    updateMask="draftGrade",
                                    body={"draftGrade": 0})
            subs = results.execute()
            print(subs)
            break

    def __init__(self):

        parser = argparse.ArgumentParser(prog='classroom',
                                         )
        # Create Subparser
        subparser = parser.add_subparsers(dest='command',
                                          metavar='',
                                          help='')

        # Account Subcommand
        account_parser = subparser.add_parser("account",
                                              help="account command")
        account_parser.add_argument('--all', '-a', action='store_true')
        account_parser.add_argument('--switch-to', '-s')
        account_parser.add_argument('--add', nargs=2,
                                    metavar=('ACC_NAME', 'PATH_TO_CRED'))
        account_parser.add_argument('--remove', '-r')

        # Courses Subcommand
        course_parser = subparser.add_parser("courses",
                                             help="courses command")
        course_parser.add_argument('--all', '-a', action='store_true')

        # Work Subcommand
        work_parser = subparser.add_parser("works",
                                           help="alias command")

        work_parser.add_argument("course", help="corse to list works")
        #emails Subcommand
        email_parser = subparser.add_parser("email",
                                           help="email command")

        email_parser.add_argument("id_aluno", help="corse to list works")
        # Alias Subcommand
        alias_parser = subparser.add_parser("alias",
                                            help="alias command")
        alias_parser.add_argument("course", help="corse to list aliases")
        group = alias_parser.add_mutually_exclusive_group()
        group.add_argument('--remove', '-r')
        group.add_argument('--add', '-a')

        # Download Subcommand
        down_parser = subparser.add_parser("download",
                                           help="download command")
        down_parser.add_argument('course')
        down_parser.add_argument('workid', nargs='?', default=None)
        down_parser.add_argument('--status')
        down_parser.add_argument('--zip', action='store_true')
        # down_parser.add_argument('--late', action='store_true')
        down_parser.add_argument('--all', '-a', action='store_true')

        # History Subcommand
        # Submission Subcommand
        subs_parser = subparser.add_parser("submissions",
                                           help="alias command")
        subs_parser.add_argument('course')
        subs_parser.add_argument('workid', nargs='?', default=None)
        subs_parser.add_argument('--status')
        subs_parser.add_argument('--late', action='store_true')
        subs_parser.add_argument('--all', '-a', action='store_true')
        subs_parser.add_argument('--id', action='store_true')

        # History Subcommand
        hist_parser = subparser.add_parser("history",
                                           help="alias command")
        hist_parser.add_argument('course')
        hist_parser.add_argument('workid')
        hist_parser.add_argument('userid')
        hist_parser.add_argument('--late', action='store_true')
        hist_parser.add_argument('--all', '-a', action='store_true')

        # Grade Subcommand
        grade_parser = subparser.add_parser("grade",
                                            help="grade command")
        grade_parser.add_argument('grader')

        # Pre-grade Subcommand
        pre_grade_parser = subparser.add_parser("pre_grade",
                                                help="grade command")
        # test grade
        test_parser = subparser.add_parser("test",
                                           help="grade command")
        test_parser.add_argument('course')
        # Auto completion
        # argcomplete.autocomplete(parser)
        # Read Input
        args = parser.parse_args()
        self.create_folder()
        sys.path.append(os.getcwd())
        if args.command == 'account' or args.command == 'grade':
            getattr(self, args.command)(args)
        else:
            try:
                self.service = build('classroom', 'v1',
                                     credentials=self._get_creds())
            except Exception as err:
                print(err)
            else:
                getattr(self, args.command)(args)


if __name__ == '__main__':
    ClassroomCli()
